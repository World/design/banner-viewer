use crate::application::Action;
use crate::banners;
use crate::config::{APP_ID, PROFILE};
use crate::widgets::banner;
use crate::window_state;
use anyhow::Result;
use gio::prelude::*;
use glib::Sender;
use gtk::prelude::*;
use libhandy::{Column, ColumnExt};
use std::cell::{Ref, RefCell};
use std::collections::HashMap;
use std::path::PathBuf;

pub enum View {
    Empty,
    Banners,
}

pub struct Window {
    pub widget: gtk::ApplicationWindow,
    sender: Sender<Action>,
    builder: gtk::Builder,
    banners_container: gtk::Box,
    settings: RefCell<gio::Settings>,
}

impl Window {
    pub fn new(sender: Sender<Action>) -> Self {
        let settings = gio::Settings::new(APP_ID);
        let builder = gtk::Builder::from_resource("/org/gnome/design/BannerViewer/window.ui");
        get_widget!(builder, gtk::ApplicationWindow, window);
        let banners_container = gtk::Box::new(gtk::Orientation::Vertical, 12);

        let window_widget = Window {
            widget: window,
            sender,
            builder,
            banners_container,
            settings: RefCell::new(settings),
        };

        window_widget.init();
        window_widget.setup_actions();
        window_widget
    }

    pub fn load_banners(&self, banners: &banners::Banners) {
        if banners.elements.borrow().len() == 0 {
            send!(self.sender, Action::SetView(View::Empty));
        } else {
            for elem in banners.elements.borrow().iter() {
                let banner_widget = banner::Banner::new(self.sender.clone(), elem.clone());
                banner_widget.widget.show();
                self.banners_container.pack_start(&banner_widget.widget, false, false, 18);
            }
            send!(self.sender, Action::SetView(View::Banners));
        }
    }

    pub fn set_view(&self, view: View) {
        get_widget!(self.builder, gtk::Stack, main_stack);
        get_widget!(self.builder, gtk::HeaderBar, headerbar);
        match view {
            View::Empty => {
                for child in self.banners_container.get_children() {
                    self.banners_container.remove(&child);
                }
                headerbar.set_has_subtitle(false);
                main_stack.set_visible_child_name("empty_state");
            }
            View::Banners => {
                headerbar.set_has_subtitle(true);
                main_stack.set_visible_child_name("banners");
            }
        }
    }

    pub fn set_loaded_file(&self, file: PathBuf) -> Result<()> {
        get_widget!(self.builder, gtk::HeaderBar, headerbar);

        let file_path = file.to_str().unwrap();

        headerbar.set_subtitle(Some(&file_path));
        self.settings.borrow_mut().set_string("last-open-file", &file_path)?;
        Ok(())
    }

    pub fn set_errors(&self, errors: Ref<HashMap<String, String>>) {
        get_widget!(self.builder, gtk::ToggleButton, errors_togglebtn);
        get_widget!(self.builder, gtk::Label, errors_count_label);

        let errors_count = errors.len();
        if errors_count != 0 {
            get_widget!(self.builder, gtk::Box, errors_container);
            // Empty the old errors
            for error_label in errors_container.get_children() {
                errors_container.remove(&error_label);
            }

            for (id, error) in errors.iter() {
                let error_box = gtk::Box::new(gtk::Orientation::Vertical, 6);
                error_box.show();

                let error_app = gtk::Label::new(Some(id));
                error_app.set_halign(gtk::Align::Start);
                error_app.get_style_context().add_class("dim-label");
                error_app.set_xalign(0.0);
                error_app.show();
                error_box.pack_start(&error_app, false, false, 0);

                let error_label = gtk::Label::new(Some(error));
                error_label.set_halign(gtk::Align::Start);
                error_label.set_property_width_request(300);
                error_label.set_xalign(0.0);
                error_label.set_property_wrap(true);
                error_label.set_ellipsize(pango::EllipsizeMode::End);
                error_label.show();
                error_box.pack_start(&error_label, false, false, 0);

                errors_container.pack_start(&error_box, false, false, 12);
            }
        }

        errors_count_label.set_text(&errors_count.to_string());
        errors_togglebtn.set_sensitive(errors_count != 0);
    }

    fn init(&self) {
        if PROFILE == "Devel" {
            self.widget.get_style_context().add_class("devel");
        }
        // setup app menu
        let menu_builder = gtk::Builder::from_resource("/org/gnome/design/BannerViewer/menu.ui");
        get_widget!(menu_builder, gio::MenuModel, popover_menu);
        get_widget!(self.builder, gtk::MenuButton, appmenu_button);
        appmenu_button.set_menu_model(Some(&popover_menu));
        // load latest window state
        let settings = self.settings.borrow().clone();
        window_state::load(&self.widget, &settings, self.sender.clone());
        // save window state on delete event
        self.widget.connect_delete_event(move |window, _| {
            if let Err(err) = window_state::save(&window, &settings) {
                warn!("Failed to save window state {}", err);
            }
            Inhibit(false)
        });

        get_widget!(self.builder, gtk::Viewport, viewport);
        let column = Column::new();
        column.set_maximum_width(1200);
        column.add(&self.banners_container);
        column.set_margin_bottom(24);
        column.show();
        self.banners_container.show();
        viewport.add(&column);

        // Errors popover
        get_widget!(self.builder, gtk::ToggleButton, errors_togglebtn);
        get_widget!(self.builder, gtk::Popover, errors_popover);
        errors_popover.connect_property_visible_notify(clone!(@weak errors_togglebtn => move |popover| {
            if !popover.get_visible() {
                errors_togglebtn.set_active(false);
            }
        }));
        errors_togglebtn.connect_toggled(move |togglebtn| {
            if togglebtn.get_active() {
                errors_popover.popup();
            } else {
                errors_popover.popdown();
            }
        });
    }

    fn setup_actions(&self) {
        let actions = gio::SimpleActionGroup::new();
        action!(
            actions,
            "open",
            clone!(@strong self.sender as sender => move |_, _| {
                send!(sender, Action::OpenFile);
            })
        );
        self.widget.insert_action_group("window", Some(&actions));
    }
}
