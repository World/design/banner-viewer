use crate::application::Action;
use crate::banners;
use glib::Sender;
use gtk::prelude::*;
use sourceview4::prelude::*;
use std::{cell::RefCell, rc::Rc};

#[derive(Debug)]
pub struct Banner {
    pub widget: gtk::Box,
    builder: gtk::Builder,
    sender: Sender<Action>,
    banner: RefCell<banners::Banner>,
}

// Contains an EventBox & Revealer(SourceView)
impl Banner {
    pub fn new(sender: Sender<Action>, banner: banners::Banner) -> Rc<Self> {
        let builder = gtk::Builder::from_resource("/org/gnome/design/BannerViewer/banner.ui");
        get_widget!(builder, gtk::Box, banner_widget);

        banner_widget.get_style_context().add_class("banner-widget");
        banner_widget.show();

        let b = Rc::new(Banner {
            widget: banner_widget,
            builder,
            sender,
            banner: RefCell::new(banner),
        });

        b.init(b.clone());
        b
    }

    fn init(&self, banner_widget: Rc<Self>) {
        get_widget!(self.builder, gtk::EventBox, banner_eventbox);

        get_widget!(self.builder, gtk::Revealer, source_revealer);
        get_widget!(self.builder, sourceview4::View, source_view);

        source_view.set_monospace(true);
        source_view.set_indent_width(2);
        source_view.show();

        get_widget!(self.builder, sourceview4::Buffer, source_buffer);

        source_revealer.show();
        banner_eventbox.show();

        // Inject the CSS into the SourceView
        let css_lang = sourceview4::LanguageManager::get_default().and_then(|lm| lm.get_language("css")).unwrap();
        source_buffer.set_language(Some(&css_lang));
        source_buffer.set_highlight_syntax(true);
        source_buffer.set_text(&self.banner.borrow().css.trim());

        if let Some(undo_manager) = source_buffer.get_undo_manager() {
            undo_manager.begin_not_undoable_action();
            undo_manager.end_not_undoable_action();
        }

        if let Some(scheme) = sourceview4::StyleSchemeManager::get_default().and_then(|scm| scm.get_scheme("solarized-light")) {
            source_buffer.set_style_scheme(Some(&scheme));
        }

        let parent = self.widget.clone();
        banner_eventbox.connect_button_press_event(move |_, _| {
            let is_revealed = source_revealer.get_reveal_child();
            source_revealer.set_reveal_child(!is_revealed);
            if !is_revealed {
                parent.get_style_context().add_class("revealed");
            } else {
                parent.get_style_context().remove_class("revealed");
            }
            gtk::Inhibit(false)
        });

        source_buffer.connect_changed(move |buffer| {
            let start = buffer.get_start_iter();
            let end = buffer.get_end_iter();

            let css = buffer.get_text(&start, &end, true).unwrap();
            banner_widget.inject_css(&css);
        });
        let css = self.banner.borrow().css.clone();
        self.inject_css(&css);
    }

    fn inject_css(&self, css: &str) {
        self.banner.borrow_mut().set_css(css);
        let sender = self.sender.clone();
        let banner = self.banner.borrow().clone();
        let ctx = glib::MainContext::default();
        ctx.spawn_local(async move {
            let error_id = format!("io_{}", banner.id);
            match banner.prepare_resources().await {
                Ok(_) => {
                    send!(sender, Action::RemoveError(error_id));
                }
                Err(err) => {
                    warn!("Failed to download the file {}", err);
                    send!(sender, Action::AddError(error_id, "Failed to download a web resource".to_string()));
                }
            };
        });
        get_widget!(self.builder, gtk::Box, banner);
        let ctx = banner.get_style_context();

        // Inject the banner CSS
        let mut css = css.replace("@datadir@/gnome-software", &self.banner.borrow().resources_dir);
        let web_resources = self.banner.borrow().web_resources.borrow().clone();
        for (url, cache_path) in web_resources.iter() {
            css = css.replace(url, cache_path);
        }

        let css = format!(".banner{{ {} }}", css);

        let banner_id = &self.banner.borrow().id;
        let provider = gtk::CssProvider::new();
        match provider.load_from_data(css.as_bytes()) {
            Ok(_) => send!(self.sender, Action::RemoveError(banner_id.to_string())),
            Err(err) => {
                warn!("Couldn't load stylesheet of banner {}", err);
                let sent_error = err.to_string().replace("<data>:", "");
                send!(self.sender, Action::AddError(banner_id.to_string(), sent_error));
            }
        };
        ctx.add_provider(&provider, 300);
    }
}
