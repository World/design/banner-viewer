use crate::banners;
use crate::config;
use crate::widgets::{View, Window};
use anyhow::Result;
use gio::prelude::*;
use gtk::prelude::*;
use std::collections::HashMap;
use std::env;
use std::path::PathBuf;
use std::{cell::RefCell, rc::Rc};

use glib::{Receiver, Sender};
pub enum Action {
    OpenFile,
    LoadFile(PathBuf),
    SetView(View),
    AddError(String, String), // (id, error)
    RemoveError(String),      // id
}

pub struct Application {
    app: gtk::Application,
    window: Window,
    sender: Sender<Action>,
    receiver: RefCell<Option<Receiver<Action>>>,
    banners: banners::Banners,
    pub errors: RefCell<HashMap<String, String>>, // (id, the error)
}

impl Application {
    pub fn new() -> Rc<Self> {
        let app = gtk::Application::new(Some(config::APP_ID), Default::default()).unwrap();

        let (sender, r) = glib::MainContext::channel(glib::PRIORITY_DEFAULT);
        let receiver = RefCell::new(Some(r));

        let banners = banners::Banners::new(sender.clone());
        let window = Window::new(sender.clone());

        let application = Rc::new(Self {
            app,
            window,
            sender,
            receiver,
            banners,
            errors: RefCell::new(HashMap::new()),
        });

        application.setup_gactions();
        application.setup_signals();
        application.setup_css();
        application
    }

    pub fn run(&self, app: Rc<Self>) {
        info!("{}Banner Viewer ({})", config::NAME_PREFIX, config::APP_ID);
        info!("Version: {} ({})", config::VERSION, config::PROFILE);
        info!("Datadir: {}", config::PKGDATADIR);

        let receiver = self.receiver.borrow_mut().take().unwrap();
        receiver.attach(None, move |action| app.do_action(action));

        let args: Vec<String> = env::args().collect();
        self.app.run(&args);
    }

    fn setup_gactions(&self) {
        // Quit
        let app = self.app.clone();
        action!(self.app, "quit", move |_, _| app.quit());
        // About
        let window = self.window.widget.clone();
        action!(self.app, "about", move |_, _| {
            let builder = gtk::Builder::from_resource("/org/gnome/design/BannerViewer/about_dialog.ui");
            get_widget!(builder, gtk::AboutDialog, about_dialog);
            about_dialog.set_transient_for(Some(&window));

            about_dialog.connect_response(|dialog, _| dialog.close());
            about_dialog.show();
        });
        // Shortcuts
        self.app.set_accels_for_action("app.quit", &["<primary>q"]);
        self.app.set_accels_for_action("window.open", &["<primary>o"]);
        self.app.set_accels_for_action("win.show-help-overlay", &["<primary>question"]);
    }

    fn setup_signals(&self) {
        let window = self.window.widget.clone();
        self.app.connect_activate(move |app| {
            window.set_application(Some(app));
            app.add_window(&window);
            window.present();
        });
        // Shortcuts Dialog
        let builder = gtk::Builder::from_resource("/org/gnome/design/BannerViewer/shortcuts.ui");
        get_widget!(builder, gtk::ShortcutsWindow, shortcuts);
        self.window.widget.set_help_overlay(Some(&shortcuts));
    }

    fn setup_css(&self) {
        let p = gtk::CssProvider::new();
        gtk::CssProvider::load_from_resource(&p, "/org/gnome/design/BannerViewer/style.css");
        if let Some(screen) = gdk::Screen::get_default() {
            gtk::StyleContext::add_provider_for_screen(&screen, &p, 500);
        }
    }

    fn open_file(&self) {
        let open_dialog = gtk::FileChooserDialog::with_buttons(
            Some("Open File"),
            Some(&self.window.widget),
            gtk::FileChooserAction::Open,
            &[("_Cancel", gtk::ResponseType::Cancel), ("_Open", gtk::ResponseType::Accept)],
        );

        let xml_filter = gtk::FileFilter::new();
        xml_filter.set_name(Some("application/xml"));
        xml_filter.add_mime_type("application/xml");
        open_dialog.add_filter(&xml_filter);

        open_dialog.connect_response(clone!(@strong self.sender as sender => move |dialog, response| {
            if response == gtk::ResponseType::Accept {
                let file = dialog.get_filename().unwrap();
                send!(sender, Action::LoadFile(file));
            }
            dialog.close();
        }));
    }

    fn load_file(&self, filepath: PathBuf) -> Result<()> {
        let file = gio::File::new_for_path(&filepath);
        self.banners.load_file(file)?;
        self.window.load_banners(&self.banners);
        self.window.set_loaded_file(filepath)?;
        Ok(())
    }

    fn do_action(&self, action: Action) -> glib::Continue {
        match action {
            Action::OpenFile => self.open_file(),
            Action::LoadFile(file) => {
                if let Err(err) = self.load_file(file) {
                    self.window.set_view(View::Empty);
                    error!("Failed to parse the file {}", err);
                }
            }
            Action::SetView(view) => self.window.set_view(view),
            Action::AddError(id, error) => {
                self.errors.borrow_mut().insert(id, error);
                self.window.set_errors(self.errors.borrow());
            }
            Action::RemoveError(id) => {
                self.errors.borrow_mut().remove(&id);
                self.window.set_errors(self.errors.borrow());
            }
        };
        glib::Continue(true)
    }
}
