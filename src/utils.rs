use anyhow::Result;
use isahc::ResponseExt;

pub async fn download(url: &str, cache: &str) -> Result<()> {
    let mut response = isahc::get_async(url).await?;
    response.copy_to_file(cache)?;
    Ok(())
}
