#[macro_use]
extern crate lazy_static;
#[macro_use]
extern crate log;
#[macro_use]
extern crate glib;
#[macro_use]
extern crate gtk_macros;

use gettextrs::*;

mod application;
mod banners;
mod config;
mod static_resources;
mod utils;
mod widgets;
mod window_state;

use application::Application;
use config::{GETTEXT_PACKAGE, LOCALEDIR};

fn main() {
    pretty_env_logger::init();

    gtk::init().expect("Unable to start GTK3");
    libhandy::init().expect("Failed to initialize libhandy");

    // Prepare i18n
    setlocale(LocaleCategory::LcAll, "");
    bindtextdomain(GETTEXT_PACKAGE, LOCALEDIR);
    textdomain(GETTEXT_PACKAGE);

    glib::set_application_name(&format!("{}Banner Viewer", config::NAME_PREFIX));
    glib::set_prgname(Some("banner-viewer"));

    static_resources::init().expect("Failed to initialize the resource file.");

    let app = Application::new();
    app.run(app.clone());
}
