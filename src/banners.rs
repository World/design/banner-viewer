use crate::application::Action;
use crate::utils;
use anyhow::Result;
use gio::prelude::*;
use glib::Sender;
use libxml::parser::Parser;
use regex::Regex;
use std::collections::HashMap;
use std::path::PathBuf;
use std::{cell::RefCell, rc::Rc};

#[derive(Debug)]
pub enum BannerError {
    ParseFailed(libxml::parser::XmlParseError),
    NoElements,
}

impl From<libxml::parser::XmlParseError> for BannerError {
    fn from(error: libxml::parser::XmlParseError) -> Self {
        BannerError::ParseFailed(error)
    }
}

impl From<()> for BannerError {
    fn from(_: ()) -> Self {
        BannerError::NoElements
    }
}

impl std::fmt::Display for BannerError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match *self {
            BannerError::NoElements => write!(f, "BannerError::NoElements"),
            BannerError::ParseFailed(_) => write!(f, "BannerError::ParseFailed"),
        }
    }
}

impl std::error::Error for BannerError {
    fn description(&self) -> &str {
        match *self {
            BannerError::ParseFailed(_) => "Failed to parse the banners file",
            BannerError::NoElements => "No elements found",
        }
    }

    fn cause(&self) -> Option<&dyn std::error::Error> {
        None
    }
}

#[derive(Debug, Clone)]
pub struct Banner {
    pub id: String,
    pub css: String,
    pub resources_dir: String,
    pub web_resources: Rc<RefCell<HashMap<String, String>>>, // (url, cache file path),
    sender: Sender<Action>,
}

impl Banner {
    pub fn new(sender: Sender<Action>, id: String, css: String, resources_dir: String) -> Self {
        let banner = Banner {
            id,
            css,
            resources_dir,
            web_resources: Rc::new(RefCell::new(HashMap::new())),
            sender,
        };

        banner.init();
        banner
    }

    fn init(&self) {
        self.prepare_css();
    }

    pub fn set_css(&mut self, css: &str) {
        self.css = css.to_string();
        self.prepare_css();
    }

    fn prepare_css(&self) {
        lazy_static! {
            static ref RE: Regex = Regex::new(r"(?:https|http)+://(?:\w\.?)+\.(?:[\w]/?\~?\-?\.?)+").expect("Invalid Regex expression");
        }
        if let Ok(mut web_resources) = self.web_resources.try_borrow_mut() {
            web_resources.clear();
            let css = self.css.clone();
            if let Some(urls_caps) = RE.captures(&css) {
                for url_cap in urls_caps.iter() {
                    let resource_url = url_cap.unwrap().as_str().to_string();
                    let url_segments: Vec<&str> = resource_url.rsplit('/').collect();
                    let resource_filename: String = (*url_segments.get(0).unwrap()).to_string();

                    let mut cache_file: PathBuf = glib::get_user_cache_dir().unwrap();
                    cache_file.push(resource_filename);
                    let cache_filepath = cache_file.to_str().unwrap().to_string();

                    web_resources.insert(resource_url, cache_filepath);
                }
            }
        }
    }

    pub async fn prepare_resources(&self) -> Result<()> {
        for (resource_url, cache_path) in self.web_resources.try_borrow()?.iter() {
            utils::download(resource_url, cache_path).await?;
        }
        Ok(())
    }
}

pub struct Banners {
    pub elements: Rc<RefCell<Vec<Banner>>>,
    sender: Sender<Action>,
}

impl Banners {
    pub fn new(sender: Sender<Action>) -> Self {
        Banners {
            elements: Rc::new(RefCell::new(Vec::new())),
            sender,
        }
    }

    pub fn load_file(&self, file: gio::File) -> Result<(), BannerError> {
        self.elements.borrow_mut().clear();

        // TODO: monitor the file
        let parser = Parser::default();
        let mut filepath = file.get_path().unwrap();
        info!("Parsing {:?}", filepath);
        let document = parser.parse_file(&filepath.to_str().unwrap())?;
        let root = document.get_root_element().ok_or(BannerError::NoElements)?;

        let components = root.findnodes("//components/component")?;
        filepath.pop(); // remove the filename for the PathBuf
        let dirname = filepath.to_str().unwrap();

        for component in components.iter() {
            let id = component.findnodes("id")?.get(0).ok_or(BannerError::NoElements)?.get_content();
            let css = component.findnodes("*/value")?.get(0).ok_or(BannerError::NoElements)?.get_content();
            let banner = Banner::new(self.sender.clone(), id, css, dirname.to_string());
            self.elements.borrow_mut().push(banner);
        }
        Ok(())
    }
}
