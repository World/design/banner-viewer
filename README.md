<a href="https://flathub.org/apps/details/org.gnome.design.BannerViewer">
<img src="https://flathub.org/assets/badges/flathub-badge-i-en.png" width="190px" />
</a>

# Banner Viewer

<img src="https://gitlab.gnome.org/World/design/banner-viewer/raw/master/data/icons/org.gnome.design.BannerViewer.svg" width="128" height="128" />

View and edit GNOME Software banners

## Screenshots

<div align="center">
![screenshot](data/resources/screenshots/screenshot1.png)
</div>

## Hack on Banner Viewer
To build the development version of Banner Viewer and hack on the code
see the [general guide](https://wiki.gnome.org/Newcomers/BuildProject)
for building GNOME apps with Flatpak and GNOME Builder.

You are expected to follow our [Code of Conduct](/code-of-conduct.md) when participating in project
spaces.
